# README

## The Core Stack

- [Next.js](https://nextjs.org) - React Framework
- [NextAuth.js](https://next-auth.js.org) - Authentication
- [Prisma](https://prisma.io) - Database ORM
- [Morse](https://gcdtech.gitlab.io/morse/) - UI Framework
- [Acorn](https://gcdtech.gitlab.io/internal/acorn/) - UI Component Library
- [tRPC](https://trpc.io) - RPC Framework (GraphQL Alternative)
- [Vitest](https://vitest.dev/) - Testing Framework

## Getting Started

Ensure you're on node 20. First, run the development server:

```bash
npm install
npm run dev
```

If you run into any issues, it might be worth deleting the entire project and cloning it down again.

## Site URLs

- [Site](http://localhost:3000)

## Environment Variables

If you want to use Google OAuth you will need to create a project in the
[Google Developer Console](https://console.developers.google.com/) and create a set of
credentials for your project.

## Seeding the Database

You can seed the database with the following command:

```bash
npm run db:seed
```

## Debugging

You can debug the application with vscode by running the `Debug Application` launch configuration.

## Running Tests

You can run the tests with the following command:

```bash
npm run test
```

We use Vitest for our testing framework, you can learn more about it [here](https://vitest.dev/).

## Running Linting

You can run the linting with the following command:

```bash
npm run lint
```

We use ESLint for our linting framework, you can learn more about it [here](https://eslint.org/).

## Deployment

TBD

## Prisma

You can run Prisma commands with the following command:

```bash
npm run db:generate # Generates your Prisma client
```

Some useful commands are:

```bash
npm run db:generate # Generates your Prisma client
npm run db:push # Pushes your schema to the database
npm run db:seed # Seeds the database with data
npm run db:studio # Opens the Prisma Studio
npm run db:nuke # Wipe the database
```
