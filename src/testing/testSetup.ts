import { type PrismaClient } from "@prisma/client";
import { vi } from "vitest";
import { mockDeep } from "vitest-mock-extended";

export const beforeEachHook = () => {
  // Reset everything
  vi.restoreAllMocks();
};

export type PrismaMock = ReturnType<typeof mockDeep<PrismaClient>>;

export const getPrismaMock = (
  mockPrisma?: (prismaMock: PrismaMock) => void
) => {
  // Mock prisma module
  const prismaMock = mockDeep<PrismaClient>();

  mockPrisma?.(prismaMock);

  return prismaMock;
};

/**
 * A simple convenience function to make it easier to mock a full type from a partial.
 *
 * For example if the production code requires a large object, but for the sake of a unit
 * test you only need to track the oject reference you can pass a partial version of the
 * object and this function will let TS think the object is complete.
 *
 * Its really just an assertion but avoids ts-expect-errors everywhere and documents
 * better what is going on.
 *
 * **This is only for side stepping TS expectations on types it _does not_ provide missing
 * properties on the object**
 *
 * @param partial
 * @returns
 */
export const mockFullTypeFromPartial = <T>(partial: unknown): T => {
  return partial as T;
};

/**
 * Prisma test guides invariably document the approach of using mock resolved values
 *
 * This works well but can be very tedious when often you just want to implement some
 * basic functionality that can stand in as a mock implementation for all the tests
 * in a suite.
 *
 * This would normally be done with prisma.customers.create.mockImplmentation but
 * the type definitions of the return value is difficult to match and it involves
 * assertions or ts-expect-error to get it to work.
 *
 * To avoid litering the tests with this type of hack, the hack is moved here into
 * well described functions.
 *
 * Use them in this way:
 *
 * prismaMethodMocks.createWithId(prisma.customer.create);
 *
 * These can be configured in a beforeEach() in your test if they are to simply
 * allow the use case to work properly. If you need to pass values specific to the
 * expectations of the test, you can simply recall this within your test functions:
 *
 * // Passing an ID of two to distinguish it from the default of "1"
 * prismaMethodMocks.createWithId(prisma.customer.create, "2");
 */
export const prismaMethodMocks = {
  /**
   * Simpy reflect the object passed as the result along with an id.
   *
   * The id can be passed or will default to "1".
   *
   * @param createFunction
   * @param id The id to return in the mocked response.
   */
  createWithId: (createFunction: CallableFunction, id?: string) => {
    if (!id) {
      id = "1";
    }

    (
      createFunction as unknown as {
        mockImplementation: (
          mockingFunction: (request: { data: object }) => unknown
        ) => void;
      }
    ).mockImplementation((request) => {
      return {
        ...request.data,
        id,
      };
    });
  },
  /**
   * For use on update functions and returns the value passed to it with
   * no side effects.
   *
   * @param updateFunction
   */
  update: (updateFunction: CallableFunction) => {
    (
      updateFunction as unknown as {
        mockImplementation: (
          mockingFunction: (request: { data: object }) => unknown
        ) => void;
      }
    ).mockImplementation((request) => {
      return request.data;
    });
  },
  /**
   * A simple mock to return any arbitrary value for any function.
   *
   * The value can be a literal value or a call back function that will receive
   * any arguments passed to the original function. It is up to you to ensure
   * the type of the return value matches the types in the production code - there
   * are unfortunately no type guard rails here.
   *
   * This is often used with the 'find' methods to return pre-resolved values or
   * arrays of values.
   *
   * @param prismaFunction
   * @param value
   */
  returnValue: (prismaFunction: CallableFunction, value: unknown) => {
    (
      prismaFunction as unknown as {
        mockImplementation: (
          mockingFunction: (...args: unknown[]) => unknown
        ) => void;
      }
    ).mockImplementation((...args: unknown[]) => {
      return value instanceof Function ? value(...args) : value;
    });
  },
};

export {};
