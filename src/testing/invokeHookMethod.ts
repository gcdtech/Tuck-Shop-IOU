import { renderHook } from "@testing-library/react";
import { TrpcTestWrapper } from "./TrpcTestWrapper";

export const invokeHookMethod = <T, Y>(hook: (props: T) => Y, props?: T): Y => {
  const { result } = renderHook(hook, {
    wrapper: TrpcTestWrapper,
    initialProps: props,
  });

  return result.current;
};
