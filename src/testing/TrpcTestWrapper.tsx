import { TRPCReactProvider } from "../trpc/react";

export const TrpcTestWrapper = (props: { children: React.ReactNode }) => {
  return <TRPCReactProvider>{props.children}</TRPCReactProvider>;
};
