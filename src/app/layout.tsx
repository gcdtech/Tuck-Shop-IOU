import { ColorSchemeScript, MantineProvider } from "@mantine/core";
import "@mantine/core/styles.css";
import "@mantine/notifications/styles.css";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
import { SessionProvider } from "next-auth/react";
import { Poppins } from "next/font/google";

const font = Poppins({
  weight: ["100", "200", "300", "400", "500", "600", "700", "800", "900"],
  subsets: ["latin"],
});

import { Notifications } from "@mantine/notifications";
import { TRPCReactProvider } from "~/trpc/react";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <head>
        <ColorSchemeScript />
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width, user-scalable=no"
        />
      </head>
      <body className={font.className}>
        <MantineProvider>
          <SessionProvider>
            <TRPCReactProvider>
              <ReactQueryDevtools
                buttonPosition="top-right"
                initialIsOpen={false}
              />
              <Notifications />
              <>{children}</>
            </TRPCReactProvider>
          </SessionProvider>
        </MantineProvider>
      </body>
    </html>
  );
}
