import { Box, Button, Container, Group, Stack, Title } from "@mantine/core";
import Link from "next/link";
import classes from "~/app/_styles/Home.module.css";
import { auth } from "../../../server/auth";

export default async function HomePage(totalPrice: number) {
  totalPrice = 45;
  const session = await auth();
  return (
    <Container size="90%" pos="relative" pb={80} py={10}>
      <Button variant="default" component={Link} href="/shop">
        BACK
      </Button>
      <Button variant="default" component={Link} href="/">
        RESTART
      </Button>
      <Stack>
        <Box pos="relative" py={42}>
          <Title
            ta={{ base: "left", sm: "center" }}
            fz={{ base: "2.25rem", sm: "5rem" }}
            mb="md"
            lts={-1}
            className={classes.title}
          >
            Total: {totalPrice}
          </Title>
          {session?.user && (
            <Group justify="center" mt="xl" py={100}>
              <Button
                fullWidth
                size="90px"
                radius="100px"
                fz="60px"
                variant="gradient"
                gradient={{ from: "orange", to: "red", deg: 135 }}
                component={Link}
                href="/"
              >
                PAY NOW
              </Button>

              <Button
                fullWidth
                size="90px"
                radius="100px"
                fz="60px"
                variant="gradient"
                gradient={{ from: "orange", to: "red", deg: 135 }}
                component={Link}
                href="/"
              >
                PAY LATER
              </Button>
            </Group>
          )}
        </Box>
      </Stack>
    </Container>
  );
}
