import { Box, Container, Stack, Title } from "@mantine/core";
import Balancer from "react-wrap-balancer";
import classes from "~/app/_styles/Home.module.css";
import { auth } from "~/server/auth";
import { TasksView } from "../../../_components/views/tasksView";

export default async function ProjectPage() {
  const session = await auth();

  return (
    <Container size="90%" pos="relative" pb={80}>
      <Stack>
        <Box pos="relative" py={52}>
          <Title
            ta={{ base: "left", sm: "center" }}
            fz={{ base: "2.25rem", sm: "3rem" }}
            mb="md"
            lts={-1}
            className={classes.title}
          >
            <Balancer>Debt</Balancer>
          </Title>

          {session?.user && <TasksView />}
        </Box>
      </Stack>
    </Container>
  );
}
