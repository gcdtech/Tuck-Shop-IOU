import { Box, Container, Stack, Title } from "@mantine/core";
import Balancer from "react-wrap-balancer";
import classes from "~/app/_styles/Home.module.css";
import { auth } from "../../../../../server/auth";
import { api } from "../../../../../trpc/server";

export default async function ProjectPage({
  params,
}: {
  params: { taskId: number };
}) {
  const session = await auth();
  const task = await api.task.getOne({ id: Number(params.taskId) });

  if (!session?.user) {
    return null;
  }

  return (
    <Container size="90%" pos="relative" pb={80}>
      <Stack>
        <Box pos="relative" py={52}>
          <Title
            ta={{ base: "left", sm: "center" }}
            fz={{ base: "2.25rem", sm: "3rem" }}
            mb="md"
            lts={-1}
            className={classes.title}
          >
            <Balancer>{task.name}</Balancer>
          </Title>

          <Box>{task.days} days</Box>
        </Box>
      </Stack>
    </Container>
  );
}
