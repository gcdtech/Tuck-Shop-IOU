import { Box, Container, Group, Stack, Title } from "@mantine/core";
import Balancer from "react-wrap-balancer";
import classes from "~/app/_styles/Home.module.css";
import { auth } from "../../server/auth";
import { Keypad } from "../_components/views/keypad";

export default async function HomePage() {
  const session = await auth();
  return (
    <Container size="90%" pos="relative" pb={80}>
      <Stack>
        <Box pos="relative" py={52}>
          <Title
            ta={{ base: "left", sm: "center" }}
            fz={{ base: "2.25rem", sm: "3rem" }}
            mb="md"
            lts={-1}
            className={classes.title}
          >
            <Balancer>GCD Tuck Shop</Balancer>
          </Title>

          <Keypad></Keypad>
          {session?.user && (
            <Group justify="center" mt="xl">
              {/* <Button component={Link} href="/tasks">
                View Tasks
              </Button> */}
            </Group>
          )}
        </Box>
      </Stack>
    </Container>
  );
}
