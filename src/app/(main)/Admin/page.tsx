"use client";

import { Box, Container, Group, Stack, Table, Title } from "@mantine/core";
import Balancer from "react-wrap-balancer";
import classes from "~/app/_styles/Home.module.css";

const users = [
  { surname: "Mulligan", forename: "Scott", pin: 1234, owed_debt: 1 },
  { surname: "Graham", forename: "Andrew", pin: 1234, owed_debt: 1 },
  { surname: "Graham", forename: "Graham", pin: 1234, owed_debt: 1 },
  { surname: "Employee", forename: "Other", pin: 1234, owed_debt: 1 },
  { surname: "Other", forename: "Employee", pin: 1234, owed_debt: 1 },
];

function TableTime() {
  const rows = users.map((user) => (
    <Table.Tr key={user.owed_debt}>
      <Table.Td>{user.surname}</Table.Td>
      <Table.Td>{user.forename}</Table.Td>
      <Table.Td>{user.pin}</Table.Td>
      <Table.Td>{user.owed_debt}</Table.Td>
    </Table.Tr>
  ));

  return (
    <Table
      striped
      highlightOnHover
      withColumnBorders
      stickyHeader
      stickyHeaderOffset={60}
    >
      <Table.Thead>
        <Table.Tr>
          <Table.Th>Surname</Table.Th>
          <Table.Th>Forename</Table.Th>
          <Table.Th>Pin</Table.Th>
          <Table.Th>Owed Debt</Table.Th>
        </Table.Tr>
      </Table.Thead>
      <Table.Tbody>{rows}</Table.Tbody>
    </Table>
  );
}

export default function HomePage() {
  return (
    <Container size="90%" pos="relative" pb={80}>
      <Stack>
        <Box pos="relative" py={20}>
          <Title
            ta={{ base: "left", sm: "center" }}
            fz={{ base: "2.25rem", sm: "3rem" }}
            mb="md"
            lts={-1}
            className={classes.title}
          >
            <Balancer>TRANSACTIONS</Balancer>
          </Title>

          {
            <Group justify="center" mt="xl">
              {TableTime()}
            </Group>
          }
        </Box>
      </Stack>
    </Container>
  );
}
