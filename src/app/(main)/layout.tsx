import {
  AppShell,
  AppShellFooter,
  AppShellHeader,
  AppShellMain,
} from "@mantine/core";
import { auth } from "../../server/auth";
import Header from "../_components/layout/header";
import Footer from "../_components/layout/footer";

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const session = await auth();
  return (
    <AppShell padding={0} header={{ height: 60 }} footer={{ height: 60 }}>
      <AppShellHeader>
        <Header userId={session?.user?.id} />
      </AppShellHeader>

      <AppShellMain>{children}</AppShellMain>

      <AppShellFooter>
        <Footer />
      </AppShellFooter>
    </AppShell>
  );
}
