"use client";
import { Alert, Button, Center, Group, Input, SimpleGrid } from "@mantine/core";
import { IconInfoCircle } from "@tabler/icons-react";
import { useState } from "react";
import { applyKeypadDesign, keypadDesign } from "./digitsDesign";

export const Keypad = () => {
  //Creating pin state
  const [pin, setPin] = useState<string>("");
  //setting alert state
  const [alert, setAlert] = useState<JSX.Element | null>(null);
  //array of valid user pins
  const validPins = ["1233", "1111", "3242", "3212"];
  //admins pins
  const adminPin = "1729";
  //assigning info icon to variable
  const icon = <IconInfoCircle />;

  //function that handles user enter
  const handleEnter = () => {
    //if pin length less than four raise alert
    if (pin.length < 4) {
      setAlert(
        <Alert
          variant="filled"
          color="red"
          withCloseButton
          title="Alert title"
          icon={icon}
          onClose={() => setAlert(null)}
        >
          Pin must be 4 digits long.
        </Alert>
      );
      //return to exit function
      return;
    }

    //check if pin is admin pin if so bring user to admin page
    if (pin === adminPin) {
      window.location.href = "/Admin";
      return;
    }

    //loop through array of acceptable pins
    for (const acceptablePin of validPins) {
      //if the pin is one of the acceptable pins
      if (pin === acceptablePin) {
        //direct user to shop page
        window.location.href = "/shop";
        // console.log("Valid Pin Entered");
        //keep alert value null
        setAlert(null);
        return;
      }
    }

    //if the user enters an incorrect pin set alert to invalid alert
    setAlert(
      <Alert
        variant="filled"
        color="red"
        withCloseButton
        title="Alert title"
        icon={icon}
        onClose={() => setAlert(null)}
      >
        Invalid Pin. Please try again.
      </Alert>
    );

    //set pin to empty string so it clears from text bar
    setPin("");
  };

  //function that handles delete button
  const deleteDigit = () => {
    //set pin to sliced pin that excludes last digit
    setPin((prevPin) => prevPin.slice(0, -1));
  };

  //function that handles button press
  const addDigit = (digit: number) => {
    //set pin to previous pin plus new digit
    setPin((prevPin) => (prevPin + digit).slice(0, 4));
  };

  //returned component
  return (
    <div>
      {/* set position for alert */}
      {alert}
      {/* input field */}
      <Input
        size="xl"
        radius="xl"
        disabled
        placeholder="Enter Pin"
        value={pin}
        id="text-input"
      />
      {/* gap between input field and button */}
      <br />

      <SimpleGrid cols={1} verticalSpacing="xs" spacing="xs">
        <Center>
          {[1, 2, 3].map((num) => (
            <Button
              {...applyKeypadDesign(keypadDesign)}
              maw={200}
              key={num}
              fullWidth
              onClick={() => addDigit(num)}
            >
              {num}
            </Button>
          ))}
        </Center>

        <Center>
          {[4, 5, 6].map((num) => (
            <Button
              {...applyKeypadDesign(keypadDesign)}
              maw={200}
              key={num}
              fullWidth
              onClick={() => addDigit(num)}
            >
              {num}
            </Button>
          ))}
        </Center>

        <Center>
          {[7, 8, 9].map((num) => (
            <Button
              {...applyKeypadDesign(keypadDesign)}
              maw={200}
              key={num}
              fullWidth
              onClick={() => addDigit(num)}
            >
              {num}
            </Button>
          ))}
        </Center>
        <Center>
          {[0].map((num) => (
            <Button
              {...applyKeypadDesign(keypadDesign)}
              maw={200}
              key={num}
              fullWidth
              onClick={() => addDigit(num)}
            >
              {num}
            </Button>
          ))}
        </Center>

        <div>
          <Group justify="center">
            <Button
              {...applyKeypadDesign(keypadDesign)}
              maw={400}
              fullWidth
              id="enter-button"
              onClick={deleteDigit}
            >
              Delete
            </Button>

            <Button
              {...applyKeypadDesign(keypadDesign)}
              maw={400}
              fullWidth
              onClick={handleEnter}
            >
              Enter
            </Button>
          </Group>
        </div>
      </SimpleGrid>
    </div>
  );
};
