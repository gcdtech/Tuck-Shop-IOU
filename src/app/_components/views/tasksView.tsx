"use client";

import { Button, Card } from "@mantine/core";
import { api } from "../../../trpc/react";

export const TasksView = () => {
  // set up our mutations and queries
  const tasks = api.task.getAll.useQuery().data ?? [];
  const addTaskMutation = api.task.create.useMutation();
  const removeTaskMutation = api.task.remove.useMutation();
  const utils = api.useUtils();

  // define our addTask function
  const addTask = async () => {
    await addTaskMutation.mutateAsync({ name: "New Task", days: 1 });
    await utils.task.invalidate();
  };

  // definte our deleteTask function
  const deleteTask = async (id: number) => {
    await removeTaskMutation.mutateAsync({ id });
    await utils.task.invalidate();
  };

  return (
    <div>
      {/* add task button */}
      <Button onClick={addTask}>Add Task</Button>

      {tasks.map((task) => (
        <Card key={task.id} shadow="xs" mb="md">
          {task.name} - {task.id}
          <Button onClick={() => deleteTask(task.id)}>Delete</Button>
        </Card>
      ))}
    </div>
  );
};
