interface KeypadProps {
  m: string;
  variant: string;
  gradient: { from: string; to: string; deg: number };
  size: string;
}

export const keypadDesign: KeypadProps = {
  m: "lg",
  variant: "gradient",
  gradient: { from: "orange", to: "red", deg: 135 },
  size: "xl",
};

export const applyKeypadDesign = (design: KeypadProps) => {
  return {
    m: design.m,
    variant: design.variant,
    gradient: design.gradient,
    size: design.size,
  };
};
