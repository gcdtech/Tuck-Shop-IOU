"use client";

import { Container, Flex, Group, Text, UnstyledButton } from "@mantine/core";
import Link from "next/link";

export default function Footer() {
  return (
    <Container h="100%" fluid={true}>
      <Group justify="space-between" w="100%" gap={0} h="100%">
        <UnstyledButton component={Link} href={"/"}>
          <Flex gap="xs" align="center">
            {/* <Image
              src="/images/logo.svg"
              alt="Tuck Shop IOU"
              width={32}
              height={32}
              priority
            /> */}
            <Text fw={900} visibleFrom="xs" size="xl">
              Tuck Shop IOU
            </Text>
          </Flex>
        </UnstyledButton>

        {/* <Group gap="xs">
          <ActionIcon
            variant="subtle"
            component={Link}
            href="https://www.facebook.com/"
            target="_blank"
            size="lg"
          >
            <IconBrandFacebook size="1.05rem" stroke={1.5} />
          </ActionIcon>
          <ActionIcon
            variant="subtle"
            component={Link}
            href="https://twitter.com/"
            target="_blank"
            size="lg"
            visibleFrom="xs"
          >
            <IconBrandTwitter size="1.05rem" stroke={1.5} />
          </ActionIcon>
          <ActionIcon
            variant="subtle"
            component={Link}
            href="https://www.youtube.com/"
            target="_blank"
            size="lg"
          >
            <IconBrandYoutube size="1.05rem" stroke={1.5} />
          </ActionIcon>

          <Anchor size="sm" component={Link} href="/" visibleFrom="xs">
            Contact Us
          </Anchor>

          <Menu shadow="md" width={200}>
            <MenuTarget>
              <Anchor size="sm">Legal</Anchor>
            </MenuTarget>
            <Menu.Dropdown>
              <Menu.Item component={Link} href="/">
                Privacy Policy
              </Menu.Item>
              <Menu.Item component={Link} href="/">
                Terms & Conditions
              </Menu.Item>
              <Menu.Item component={Link} href="/">
                Cookie Policy
              </Menu.Item>
              <Menu.Item component={Link} href="/">
                Disclaimer
              </Menu.Item>
            </Menu.Dropdown>
          </Menu>
        </Group>*/}
      </Group>
    </Container>
  );
}
