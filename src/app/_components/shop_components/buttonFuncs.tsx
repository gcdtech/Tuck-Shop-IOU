interface ButtonProps {
  size: string;
  variant: string;
  gradient: { from: string; to: string; deg: number };
  radius: string;
  fz: string;
  m: string;
}

export const buttonDesign: ButtonProps = {
  size: "lg",
  variant: "gradient",
  gradient: { from: "orange", to: "red", deg: 135 },
  radius: "xl",
  fz: "80px",
  m: "xl",
};

export const applyButtonDesign = (design: ButtonProps) => {
  return {
    size: design.size,
    variant: design.variant,
    gradient: design.gradient,
    radius: design.radius,
    style: { fontSize: design.fz },
    m: design.m,
  };
};
