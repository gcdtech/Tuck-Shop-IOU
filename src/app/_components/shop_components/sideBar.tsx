interface SidebarProps {
    size: string;
    variant: string;
    gradient: { from: string; to: string; deg: number };
  }
  
  export const sidebarDesign: SidebarProps = {
    size: "lg",
    variant: "gradient",
    gradient: { from: "orange", to: "red", deg: 135 },
  };
  
  export const applySidebarDesign = (design: SidebarProps) => {
    return {
      size: design.size,
      variant: design.variant,
      gradient: design.gradient,
    };
  };