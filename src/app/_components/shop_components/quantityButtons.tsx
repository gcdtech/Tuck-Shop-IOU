"use client";

import {
  AppShell,
  Button,
  Container,
  MantineProvider,
  Stack,
  Title,
} from "@mantine/core";
import Link from "next/link";
import { useState } from "react";
import classes from "~/app/_styles/Home.module.css";
import { applyButtonDesign, buttonDesign } from "./buttonFuncs";

export const ShopComponents = () => {
  const [total, adjustTotal] = useState(0);

  return (
    <MantineProvider
      theme={{
        fontFamily: "Lucida Console",
        headings: { fontFamily: "Lucida Console" },
      }}
    >
      <Title
        ta={{ base: "left", sm: "center" }}
        fz={{ base: "2.25rem", sm: "3rem" }}
        mb="md"
        lts={10}
        className={classes.title}
      >
        <div>
          <Container size="90%" pos="relative" pb={80}>
            <Stack>
              <AppShell
                header={{ height: 60 }}
                navbar={{
                  width: 300,
                  breakpoint: "sm",
                }}
                padding="md"
              >
                <AppShell.Navbar p="md" bg="orange">
                  DEBT
                  <Button radius="md" fz="30px" size="md" color="gray">
                    PAY
                  </Button>
                  <div>PIN</div>
                </AppShell.Navbar>
              </AppShell>

              <h1>
                <Button
                  onClick={() => {
                    if (total <= 0) adjustTotal(0);
                    else adjustTotal(total - 0.2);
                  }}
                  {...applyButtonDesign(buttonDesign)}
                >
                  -
                </Button>
                20p
                <Button
                  onClick={() => adjustTotal(total + 0.2)}
                  {...applyButtonDesign(buttonDesign)}
                >
                  +
                </Button>
              </h1>
              <h1>
                <Button
                  onClick={() => {
                    if (total <= 0) adjustTotal(0);
                    else adjustTotal(total - 0.4);
                  }}
                  {...applyButtonDesign(buttonDesign)}
                >
                  -
                </Button>
                40p
                <Button
                  onClick={() => adjustTotal(total + 0.4)}
                  {...applyButtonDesign(buttonDesign)}
                >
                  +
                </Button>
              </h1>
              <h1>
                <Button
                  onClick={() => {
                    if (total <= 0) adjustTotal(0);
                    else adjustTotal(total - 0.5);
                  }}
                  {...applyButtonDesign(buttonDesign)}
                >
                  -
                </Button>
                50p
                <Button
                  onClick={() => adjustTotal(total + 0.5)}
                  {...applyButtonDesign(buttonDesign)}
                >
                  +
                </Button>
              </h1>
              <Button
                variant="gradient"
                gradient={{ from: "orange", to: "red", deg: 135 }}
                radius="xs"
                fz="40px"
                size="xl"
                component={Link}
                href="/Checkout"
              >
                {total.toFixed(2)}
              </Button>
              <Button
                variant="gradient"
                gradient={{ from: "orange", to: "red", deg: 135 }}
                radius="xl"
                fz="30px"
                size="xl"
                onClick={() => adjustTotal(0)}
                mt="lg"
              >
                RESET
              </Button>
            </Stack>
          </Container>
        </div>
      </Title>
    </MantineProvider>
  );
};
