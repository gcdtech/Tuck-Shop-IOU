import { z } from "zod";

export const CreateTaskSchema = z.object({
  name: z.string().min(3, "Task name cannot be less than 3 characters"),
  days: z.number().min(0.25, "Days cannot be less than 0.25"),
  state: z.string().optional().default("Not Started"),
});

export type CreateTaskType = z.input<typeof CreateTaskSchema>;

export const UpdateTaskSchema = z.object({
  id: z.number().positive(),
  name: z.string().min(3, "Task name cannot be less than 3 characters"),
  days: z.number().min(0.24, "Days cannot be less than 0.25"),
  state: z.string().optional().default("Not Started"),
});

export type UpdateTaskType = z.input<typeof UpdateTaskSchema>;

export const RemoveTaskSchema = z.object({
  id: z.number().positive(),
});

export type RemoveTaskType = z.input<typeof RemoveTaskSchema>;

export const AssignTaskSchema = z.object({
  sprintId: z.number(),
  teamMateId: z.number(),
  taskId: z.number().positive(),
});
