import { z } from "zod";
import { createTRPCRouter, protectedProcedure } from "~/server/api/trpc";
import { CreateTaskUseCase } from "../../logic/task/CreateTaskUseCase";
import { GetAllTasksUseCase } from "../../logic/task/GetAllTaskUseCase";
import { GetOneTaskUseCase } from "../../logic/task/GetOneTaskUseCase";
import { RemoveTaskUseCase } from "../../logic/task/RemoveTaskUseCase";
import { UpdateTaskUseCase } from "../../logic/task/UpdateTaskUseCase";
import {
  CreateTaskSchema,
  RemoveTaskSchema,
  UpdateTaskSchema,
} from "../schemas/task";

export const taskRouter = createTRPCRouter({
  getAll: protectedProcedure.query(async ({ ctx }) => {
    const getAllTasks = new GetAllTasksUseCase(ctx.db);
    const tasks = await getAllTasks.execute();
    return tasks;
  }),
  getOne: protectedProcedure
    .input(z.object({ id: z.number() }))
    .query(async ({ ctx, input }) => {
      const getOneTask = new GetOneTaskUseCase(ctx.db);
      const task = await getOneTask.execute(input);
      return task;
    }),
  create: protectedProcedure
    .input(CreateTaskSchema)
    .mutation(async ({ ctx, input }) => {
      const createTask = new CreateTaskUseCase(ctx.db);
      const task = await createTask.execute(input);
      return task;
    }),
  update: protectedProcedure
    .input(UpdateTaskSchema)
    .mutation(async ({ ctx, input }) => {
      const updateTask = new UpdateTaskUseCase(ctx.db);
      const task = await updateTask.execute(input);
      return task;
    }),
  remove: protectedProcedure
    .input(RemoveTaskSchema)
    .mutation(async ({ ctx, input }) => {
      const removeTask = new RemoveTaskUseCase(ctx.db);
      const deletedRecord = await removeTask.execute(input);
      return deletedRecord;
    }),
});
