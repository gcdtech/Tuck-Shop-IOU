/* eslint-disable @typescript-eslint/no-explicit-any */
import { type PrismaClient } from "@prisma/client";
import { db as prisma } from "../db";

export class UseCase {
  constructor(protected db: PrismaClient = prisma) {}
}

export class AuthenticatedUseCase {
  constructor(
    protected db: PrismaClient = prisma,
    protected user: { id: number }
  ) {}
}
export type getReturnType<T extends { execute: (...args: any[]) => any }> =
  ReturnType<T["execute"]> extends Promise<infer U> ? U : never;

export type getInputType<T extends { execute: (input: any) => any }> =
  Parameters<T["execute"]>[0];
