import { UseCase, type getInputType, type getReturnType } from "../useCase";

export class GetAllTasksUseCase extends UseCase {
  async execute() {
    const tasks = await this.db.task.findMany({
      orderBy: { updatedAt: "desc" },
      select: { id: true, name: true, days: true, state: true },
    });

    return tasks;
  }
}

export type GetAllTasksType = getReturnType<GetAllTasksUseCase>;
export type GetAllTasksInput = getInputType<GetAllTasksUseCase>;
