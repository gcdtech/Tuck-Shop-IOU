import { RecordNotFoundError } from "../../errors/RecordNotFoundError";
import { UseCase, type getReturnType } from "../useCase";

export class GetOneTaskUseCase extends UseCase {
  async execute(input: { id: number }) {
    const task = await this.db.task.findUnique({
      where: { id: input.id },
      select: {
        id: true,
        name: true,
        days: true,
        state: true,
      },
    });

    if (!task) {
      throw new RecordNotFoundError("Task not found");
    }

    return task;
  }
}

export type GetOneTaskType = getReturnType<GetOneTaskUseCase>;
export type GetOneTaskInput = Parameters<GetOneTaskUseCase["execute"]>[0];
