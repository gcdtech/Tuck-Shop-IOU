import { RecordNotFoundError } from "../../errors/RecordNotFoundError";
import { UseCase, type getInputType, type getReturnType } from "../useCase";

export class RemoveTaskUseCase extends UseCase {
  async execute(input: { id: number }) {
    try {
      const deletedRecord = await this.db.task.delete({
        where: { id: input.id },
        select: { id: true },
      });

      return deletedRecord;
    } catch (error) {
      throw new RecordNotFoundError("Task not found");
    }
  }
}

export type RemoveTaskType = getReturnType<RemoveTaskUseCase>;
export type RemoveTaskInput = getInputType<RemoveTaskUseCase>;
