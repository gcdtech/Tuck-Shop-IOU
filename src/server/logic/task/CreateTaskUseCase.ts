import { UseCase, type getReturnType } from "../useCase";

export class CreateTaskUseCase extends UseCase {
  async execute(input: { name: string; days: number; state: string }) {
    const task = await this.db.task.create({
      data: input,
      select: { id: true },
    });

    return task;
  }
}

export type CreateTaskType = getReturnType<CreateTaskUseCase>;
export type CreateTaskInput = Parameters<CreateTaskUseCase["execute"]>[0];
