import { RecordNotFoundError } from "../../errors/RecordNotFoundError";
import { UseCase, type getReturnType } from "../useCase";

export class UpdateTaskUseCase extends UseCase {
  async execute(input: {
    id: number;
    name: string;
    days: number;
    state: string;
  }) {
    try {
      const task = await this.db.task.update({
        where: { id: input.id },
        data: input,
      });

      return task;
    } catch (error) {
      throw new RecordNotFoundError("Task not found");
    }
  }
}

export type UpdateTaskType = getReturnType<UpdateTaskUseCase>;
export type UpdateTaskInput = Parameters<UpdateTaskUseCase["execute"]>[0];
